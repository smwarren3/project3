% EOC 6189 Project 3
% S. Matthew Warren
% Due 14 March 2019

Lx = 2.*pi; Ly = Lx;

Nx = 31; Ny = Nx;
h = Lx/Nx;

dt = 10^-2;
tmax = 10;
t = 0:dt:tmax;

x = linspace(0,Lx,Nx); y = linspace(0,Ly,Ny);
x=[x(length(x)-1),x,x(2)];y=[y(length(y)-1),y,y(2)];

phi=zeros([length(x) length(y) length(t)]);
u=phi;v=phi;

for i=1:length(t)
u(:,:,i)=(cos(x)'*sin(y)).*sin(2*pi*t(i)/tmax); 
v(:,:,i)=(sin(x)'.*cos(y)).*-sin(2*pi*t(i)/tmax);
end
phi(:,:,1) = ones(length(x)).*cos(x);

%%
for n=1:length(t)-1

    for i = 2:length(x)-1
    for j = 2:length(y)-1
        if u(i,j,n)<=0 && v(i,j,n)<=0
                phi(i,j,n+1)=(dt./h).*((u(i,j,n).*(phi(i,j,n)-phi(i-1,j,n))...
                    +v(i,j,n).*(phi(i,j,n)-phi(i,j-1,n))))+phi(i,j,n);
        else if u(i,j,n)>0 && v(i,j,n)<=0
                phi(i,j,n+1)=(dt./h).*((u(i,j,n).*(phi(i+1,j,n)-phi(i,j,n))...
                    +v(i,j).*(phi(i,j,n)-phi(i,j-1,n))))+phi(i,j,n);         
        else if u(i,j,n)<=0 && v(i,j,n)>0
                phi(i,j,n+1)=(dt./h).*((u(i,j,n).*(phi(i,j,n)-phi(i-1,j,n))...
                    +v(i,j,n).*(phi(i,j+1,n)-phi(i,j,n))))+phi(i,j,n); 
        else
                phi(i,j,n+1)=(dt./h).*(u(i,j,n).*(phi(i+1,j,n)-phi(i,j,n))...
                    +v(i,j,n).*(phi(i,j+1,n)-phi(i,j,n)))+phi(i,j,n); 
            end
            end
        end
    end
    end
end

% delete the ghost first and last columns/rows
truncate = [1 length(x)];
u(truncate,:,:)=[];u(:,truncate,:)=[];
v(truncate,:,:)=[];v(:,truncate,:)=[];
x(truncate)=[];
y(truncate)=[];
phi(truncate,:,:)=[];phi(:,truncate,:)=[];


%% Video
figure(4)
vidObj = VideoWriter('Project3b2.mp4', 'MPEG-4');
open(vidObj);
a = tic;
for k = 1:length(t)
    contourf(x,y,phi(:,:,k));colormap(jet);colorbar('Ticks',[-1,-0.5,0,0.5,1],...
         'TickLabels',{'-1','0.5','0','0.5','1'})
    axis 'tight'
    xlabel('x')
    ylabel('y')
    title('Space and Time Varying Velocity Profile')
    b = toc(a); % check timer
    if b > (1/100) % Slow down the drawing speed by a factor in the denominator
        drawnow
         M(k) = getframe(gcf);
         writeVideo(vidObj,M(k));
         a = tic; % reset timer after updating
    end
end
close(vidObj);


%%
figure(5)
vidObj = VideoWriter('TEST4.mp4', 'MPEG-4');
open(vidObj);
for k = 1:length(t)
contourf(x,y,phi(:,:,k));caxis([-1 1]);colormap(jet);colorbar;hold on
quiver(x,y,u(:,:,k),v(:,:,k),sin(2*pi*t(k)/tmax),'k'); hold off
xlabel('x')
ylabel('y')
title('Time Varying Velocity Profile')
N(k) = getframe(gcf);
writeVideo(vidObj,N(k))
end
close(vidObj);