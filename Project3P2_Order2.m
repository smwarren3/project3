% EOC 6189 Project 3
% S. Matthew Warren
% Due 14 March 2019

Lx = 2.*pi;
Ly = Lx;

Nx = 31;
Ny = Nx;

h = Lx/Nx;

x = linspace(0,Lx,Nx);
y = linspace(0,Ly,Ny);
    
u = cos(x)'.*sin(y); %eq 1
v = -sin(x)'.*cos(y);% eq 2

%{
% Quiver plot for part b
figure(1)
quiver(x,y,u,v)
title('2D velocity profile');
ylabel('y')
xlabel('x')
axis 'tight'
%}


phi_initial=ones(length(x)).*cos(x);

%{
figure(2)
contourf(x,y,phi)
title('Phi_0 = cos(x)')
xlabel('x')
ylabel('y')
axis 'tight'
%}


figure(3)
contourf(x,y,phi_initial);hold on
quiver(x,y,u,v,'k');
title('2D velocity profile');
ylabel('y')
xlabel('x')
axis 'tight'
hold off


%%

dt = 10^-2;
tmax = 10;
t = 0:dt:tmax;

%Create wrapping ghost cells for periodic boundary condition
x=[x(length(x)-2),x(length(x)-1),x,x(2),x(3)];
y=[y(length(y)-2),y(length(y)-1),y,y(2),y(3)];
phi=zeros([length(x) length(y) length(t)]);
u=phi;v=phi;

for i=1:length(t)
u(:,:,i)=(cos(x)'.*sin(y)).*sin(2*pi*t(i)/tmax); 
v(:,:,i)=(sin(x)'.*cos(y)).*-sin(2*pi*t(i)/tmax);
end

phi(:,:,1) = ones(length(x)).*cos(x);

for n=1:length(t)-1
% ileft = u(i,j).*(phi(i,j,n)-Phi(i-1,j,n));
% iright = u(i,j).*(phi(i+1,j,n)-Phi(i,j,n));
% jleft = v(i,j).*(phi(i,j,n)-phi(i,j-1,n));
% jright = v(i,j).*(phi(i,j+1,n)-phi(i,j,n));

    for i = 3:length(x)-2
    for j = 3:length(y)-2
        if u(i,j,n)<=0 && v(i,j,n)<=0
                phi(i,j,n+1)=(dt./(2.*h)).*...
                    (u(i,j,n).*(3.*phi(i,j,n)-4.*phi(i-1,j,n)+phi(i-2,j,n))...
                    +v(i,j,n).*(3.*phi(i,j,n)-4.*phi(i,j-1,n)+phi(i,j-2,n)))...
                    +phi(i,j,n);
        else if u(i,j,n)>0 && v(i,j,n)<=0
                phi(i,j,n+1)=(dt./(2.*h)).*...
                    (u(i,j,n).*(-3.*phi(i,j,n)+4.*phi(i+1,j,n)-phi(i+2,j,n))...
                    +v(i,j,n).*(3.*phi(i,j,n)-4.*phi(i,j-1,n)+phi(i,j-2,n)))...
                    +phi(i,j,n);       
        else if u(i,j,n)<=0 && v(i,j,n)>0
                phi(i,j,n+1)=(dt./(2.*h)).*...
                    (u(i,j,n).*(3.*phi(i,j,n)-4.*phi(i-1,j,n)+phi(i-2,j,n))...
                    +v(i,j,n).*(-3.*phi(i,j,n)+4.*phi(i,j+1,n)-phi(i,j+2,n)))...
                    +phi(i,j,n);
        else
                phi(i,j,n+1)=(dt./(2.*h)).*...
                    (u(i,j,n).*(-3.*phi(i,j,n)+4.*phi(i+1,j,n)-phi(i+2,j,n))...
                    +v(i,j,n).*(-3.*phi(i,j,n)+4.*phi(i,j+1,n)-phi(i,j+2,n)))...
                    +phi(i,j,n);
            end
            end
        end
    end
    end
end

% delete the ghost first and last columns/rows
truncate = [1 2 length(x)-1 length(x)];
u(truncate,:,:)=[];u(:,truncate,:)=[];
v(truncate,:,:)=[];v(:,truncate,:)=[];
x(truncate)=[];
y(truncate)=[];
phi(truncate,:,:)=[];phi(:,truncate,:)=[];

%% Video
figure(4)
vidObj = VideoWriter('TV31Points2ndOrder.mp4', 'MPEG-4');
open(vidObj);

a = tic;
for k = 1:length(t)
    contourf(x,y,phi(:,:,k));caxis([-1 1]);colormap(jet);colorbar;hold on
    quiver(x,y,u(:,:,k),v(:,:,k),sin(2*pi*t(k)/tmax),'k'); hold off
    axis 'tight'
    xlabel('x');ylabel('y');title('Time-variant velocity profile N=31 2nd Order')
        drawnow
         M(k) = getframe(gcf);
         writeVideo(vidObj,M(k));
end
close(vidObj);